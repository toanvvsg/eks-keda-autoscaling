# Architecture
![plot](./images/architect.png)

## Security Controls
- All workloads, such as EKS cluster, RDS, and Redis, will be hosted on private subnets across multiple AZs to prevent access from the internet, except for NAT gateway and Elastic Load Balancer.
- The application can access the internet via NAT Gateway.
- Network access control lists (ACLs) and security groups will be used to control inbound and outbound traffic at the subnet and ENI level, respectively.
- **K8s Network Policy** will be used to control network policy. Only Ingress pod can access frontend services, and only frontend pods can access backend services. AWS Security Group will be used to control access from Backend Pods to databases.
- Pods must run with non-root users and restricted write permissions.
- **Identity and Access Management (IAM)** will be used to manage access to AWS resources and services. Service Account and assume role will be used to prevent AWS static keys.
- **AWS SSM** will be used to store the application's secrets.
- Data encryption at rest will be implemented using **AWS Key Management Service (KMS)**.
- **RBAC** will be used to control access from team members to k8s resources and APIs.
- Utilize AWS SSM Session Manager to access to EC2 instances instead of ssh key.

## Autoscaling
- KEDA (Kubernetes Event-Driven Autoscaling) and Prometheus for autoscaling deployments.
- Prometheus provides application metrics, and KEDA performs PromQL query `http_requests_total` on Prometheus and transforms this metric to provide custom metric for HPA (Horizontal Pod Autoscaling)
- HPA scales deployment up and down based on custom metrics and our threshold configuration.
- **Karpenter** for cluster autoscaling.

## Load Balancing
- An **Elastic Load Balancer** will be used to distribute incoming traffic across multiple Nginx Ingress Daemonset accross mutiple Availability Zones.
- Since our application is a web application, we can choose an Application Load Balancer. Additionally, by integrating the Application Load Balancer with WAF, we can improve our web application security.

## Network Topology
- The VPC will be designed with public and private subnets to provide a secure network topology.
- The public subnets will host the Application Load Balancer and the NAT Gateway, while the private subnets will host the application workloads and the databases.
- A Virtual Private Network (VPN) connection or Direct Connect can be used to securely connect the VPC to an on-premises data center.

## Logging
- **AWS CloudTrail** will be used to log API calls and related events made by or on behalf of the web application in AWS.
- **Amazon CloudWatch Logs** will be used to monitor, store, and access log files from Pods and other AWS resources.

## Metrics
- **Prometheus** used to collect metrics of pods, eks cluster.
- **Amazon CloudWatch** will be used to collect and track metrics, collect and monitor log files, set alarms, and automatically react to changes in the AWS resources.

## Alerts
- Use Grafana Alert rules with Prometheus and CloudWatch Metrics data sources.
- Pingdom alert to track services uptime.
- Pagerduty to enable DevOps/SRE/Team members to react quickly to the incidents.
- Emails for application's exceptions.

## Backup Policies
- ETCD backup with Velero
- RDS point in time recovery.
- Infrastructure as code enable us another backup solution to recover from failure.

## Failover Mechanism
- The web application instances will be launched in multiple Availability Zones to provide high availability.
- Amazon Route 53 can be used as a DNS service to route traffic to healthy endpoints or to failover to another region in case of an outage.
- Use Infrastructure as code enables us to have another backup solution, to recover from failure.
- Enable RDS Multi-Az to failover quickly to standby master database in the event of an incident.

## Cost Optimization
- Use Reserved Instance and Spot Instance for EKS node pools and config less critical workload to use Spot Instance.
- Use AWS Gateway Endpoint to reduce network cost of S3, Lambda.
- Use AWS Cost Explorer to optimize cloud cost by analyzing resource utilization.
- Depend on load we can consider using serverless service to reduce cost.
- Set retention policy for S3, ECR to save cost.
- Using cache such as CloudFront, Redis to improve response time and reduce compute resources, for spike workload we can consider using
queue instead of scale-up to a larger instance.
